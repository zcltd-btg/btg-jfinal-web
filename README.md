#btg-jfinal-web

#### 项目介绍
> 基于jar包的web插件示例

#### 升级记录
```
v4.0.1
1、转为独立maven依赖，去掉parent；

v3.0.3
1、btg-parent升级到v2.0.1；

v3.0.2
统一依赖管理

v3.0.1
统一迁移至公司名下
```
